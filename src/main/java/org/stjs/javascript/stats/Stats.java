package org.stjs.javascript.stats;

import org.stjs.javascript.Array;
import org.stjs.javascript.annotation.STJSBridge;
import org.stjs.javascript.annotation.Namespace;
import org.stjs.javascript.dom.Element;

@STJSBridge
public class Stats {
    public static int REVISION;
    public static Element domElement;

    public static native void setMode(int mode);
    public static native void begin();
    public static native long end();
    public static native void update();
}